package net.oschina.git.utils;

public class RemoteApi {
	public static final String defaultHost = "git.oschina.net";
	public static final String login = "https://"+defaultHost+"/api/v3/session";
	public static final String createProject = "https://"+defaultHost+"/api/v3/projects";
	public static final String getProjects = "https://"+defaultHost+"/api/v3/projects";
	public static final String helpUrl = "http://www.oschina.net/news/74774/gitosc-eclipse-plugin";
	public static final String SignUrl = "http://git.oschina.net/";
}
