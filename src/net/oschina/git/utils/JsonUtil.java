package net.oschina.git.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JsonUtil {
	public static String getJsonValueFromStr(String str,String key){
		Gson gson = new Gson();
		JsonElement jelem = gson.fromJson(str, JsonElement.class);
		JsonObject obj = jelem.getAsJsonObject(); 
		if(obj.get(key)==null){
			return null;
		}
		return obj.get(key).getAsString();
	}
	public static JsonArray formatToJsonArray(String str){
		Gson gson = new Gson();
		JsonElement jelem = gson.fromJson(str, JsonElement.class);
		return jelem.getAsJsonArray();
	}
}
